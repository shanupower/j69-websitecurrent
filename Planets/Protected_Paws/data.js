var APP_DATA = {
    scenes: [
        {
            id: "0-first-room",
            name: "First Room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: 2.447607647925624,
                pitch: 0.0851035105350526,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: -0.22226443917490712,
                    pitch: 0.2846670986562536,
                    rotation: 5.497787143782138,
                    target: "1-second-room",
                },
                {
                    yaw: 0.29975319159350633,
                    pitch: 0.2909177910294609,
                    rotation: 0.7853981633974483,
                    target: "10-eleventh-room",
                },
            ],
            infoHotspots: [
                {
                    yaw: 1.5459492601860996,
                    pitch: -0.23929514799464613,
                    title: "Raju",
                    text: "Never have you ever seen a smaller adolescent puppy than Raju! You’ll always find him running around and playing with his shelter buddies. One of his quirks is begging his humans for a treat, and then scurrying away when one is given to him. Many would say his art is one of a kind, which is why you’d better hurry before it gets auctioned away!",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL1.jpg",
                },
            ],
            opensea:[
                {
                    yaw: -1.5459492601860996*2,
                    pitch: -0.23929514799464613,  
                }
            ]

        },
        {
            id: "1-second-room",
            name: "Second Room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: -3.0207909818550043,
                pitch: 0.00929991458684043,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: 0.7599954763863845,
                    pitch: 0.21248369994290073,
                    rotation: 13.351768777756625,
                    target: "0-first-room",
                },
                {
                    yaw: -0.7868785139556902,
                    pitch: 0.21220416868843905,
                    rotation: 5.497787143782138,
                    target: "2-third-room",
                },
            ],
            infoHotspots: [
                {
                    yaw: 2.4977283834599042,
                    pitch: -0.2772742100912069,
                    title: "Cooper",
                    text: "Cooper is the grandfather of the lot, often choosing to spend his time commanding the respect of the young ones and laying down the law of the shelter. He rules with an iron fist, which he often wields from his favourite perch, and which doubles up as his daydreaming throne. One might even say that his artwork commands the attention of the room.",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL2.jpg",
                },
            ],
            
            opensea:[
                {
                    yaw: -2.4977283834599042,
                    pitch: -0.2772742100912069,
                }
            ],

        },
        {
            id: "2-third-room",
            name: "Third Room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: -3.0586062474880826,
                pitch: 0.06749046743565579,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: 0.7755301378086461,
                    pitch: 0.2416458274263018,
                    rotation: 14.137166941154074,
                    target: "1-second-room",
                },
                {
                    yaw: -0.7613071911478908,
                    pitch: 0.21913681386708284,
                    rotation: 4.71238898038469,
                    target: "3-fourth-room",
                },
            ],
            infoHotspots: [
                {
                    yaw: 2.4985241710767507,
                    pitch: -0.2724882825875756,
                    title: "Maple",
                    text: "Maple found himself limping down the streets one day, injured and battered by the cruel world. The nightmares he suffered created a deep distrust for mankind, which made his rescue all the more challenging. Now, all he sees are rainbows and treats, which he lavishes at his new shelter. Sometimes, you can even see a hint of those rainbows in his paintings!",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL3.jpg",
                },
            ],
            opensea:[
                {
                    yaw: -2.4985241710767507,
                    pitch: -0.2724882825875756,
                }
            ]

        },
        {
            id: "3-fourth-room",
            name: "Fourth Room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: -2.3315696176881477,
                pitch: 0.24893189576589947,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: -0.28411284054260477,
                    pitch: 0.2713907397401112,
                    rotation: 36.91371367968008,
                    target: "2-third-room",
                },
                {
                    yaw: 0.36513653945095115,
                    pitch: 0.2855236568097457,
                    rotation: 0.7853981633974483,
                    target: "4-fifth-room-",
                },
            ],
            infoHotspots: [
                {
                    yaw: -3.129196363607825,
                    pitch: -0.24465680493715247,
                    title: "Mowgli",
                    text: "Mowgli, much like his namesake, sprouted like a seed, being a rescue from the jungle. His wild and untamed upbringing initially rendered him an outcast at his shelter, with him only being able to find solace in his sister. He had to bid adieu to her when he was adopted, but found himself returned to the shelter and reunited with his kin. He emboldens his art with emotions from his past, and you might even find yourself shedding a tear when you gaze upon his art.",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL4.jpg",
                },
            ],
            opensea:[
                {
                    yaw: 3.129196363607825+1.6,
                    pitch: -0.24465680493715247,   
                }
            ]
        },
        {
            id: "4-fifth-room-",
            name: "Fifth Room ",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: 3.1386560379517814,
                pitch: 0.17391086148010437,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: 0.7881238184917425,
                    pitch: 0.26028599886608994,
                    rotation: 7.0685834705770345,
                    target: "3-fourth-room",
                },
                {
                    yaw: -0.8542047321818629,
                    pitch: 0.2539592441648111,
                    rotation: 5.497787143782138,
                    target: "4-fifth-room-",
                },
            ],
            infoHotspots: [
                {
                    yaw: 2.473617108893232,
                    pitch: -0.2648921453145583,
                    title: "Babu",
                    text: "Babu is one of the free spirits at the shelter, whose unbounded happiness spreads cheer among those he meets! A city street boy, he hasn’t let the doggy dog world dampen his spirits, and is equally loved by children and dogs alike. Babu, like Clark Kent, turns superhero whenever he hears a lost and crying child, and will turn that frown upside down in lickety-split! Babu now canters around his new home, joyfully bouncing whenever it’s time to play.",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL5.jpg",
                },
            ],
            opensea:[
                {
                    yaw: -2.473617108893232,
                    pitch: -0.2648921453145583,   
                }
            ]
        },
        {
            id: "5-sixth-room",
            name: "Sixth Room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: -3.0924948935169425,
                pitch: 0.0978315766994946,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: 0.758443198210859,
                    pitch: 0.3073331087488693,
                    rotation: 7.0685834705770345,
                    target: "4-fifth-room-",
                },
                {
                    yaw: -0.7987121571269018,
                    pitch: 0.2633466450941615,
                    rotation: 5.497787143782138,
                    target: "6-seventh-room",
                },
            ],
            infoHotspots: [
                {
                    yaw: 2.4487757346454853,
                    pitch: -0.2643435385982684,
                    title: "Yogi",
                    text: "It is hard to determine whether the meaning or the dog came first when it comes to Yogi. Yogi, unlike his brethren, inspires tranquillity, and should you ever see him in a garden, you will truly picture Zen. His afternoon naps could even count as a meditation session, and his paintings might even enchant you into a snoozing trance! Safe to say, you don’t want to sleep on his art.",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL6.jpg",
                },
            ],
            opensea:[
                {
                    yaw: -2.4487757346454853,
                    pitch: -0.2643435385982684,  
                }
            ]
        },
        {
            id: "6-seventh-room",
            name: "Seventh Room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: -2.2674595000652484,
                pitch: 0.01653545617547003,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: -0.3375831094752346,
                    pitch: 0.322176705635691,
                    rotation: 11.780972450961727,
                    target: "5-sixth-room",
                },
                {
                    yaw: 0.28971486533178137,
                    pitch: 0.3060841378684067,
                    rotation: 0.7853981633974483,
                    target: "7-eighth-room",
                },
            ],
            infoHotspots: [
                {
                    yaw: -3.1217106826903667,
                    pitch: -0.2437502697198326,
                    title: "Bubba",
                    text: "Bubba has been a mother far too many times, a burden that has taken a toll on her body and mind. Her fragile state made her rescue tough, as she nearly ran away at the sight of the shelter staff. Now that her motherhood days are over, Bubba finds peace in exploring the many wonders of the shelter, and of pursuing a few passions of her own. Art has been a great pursuit to Bubba, being one of the factors catalysing her regained confidence. You will find that her pieces resonate a calm but unwavering aura of hope.",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL7.jpg",
                },
            ],
            opensea:[
                {
                    yaw: 3.1217106826903667+1.6,
                    pitch: -0.2437502697198326,  
                }
            ]
        },
        {
            id: "7-eighth-room",
            name: "Eighth Room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: -3.093401146556234,
                pitch: -0.015841649170663885,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: 0.6956857535477106,
                    pitch: 0.2844639272800116,
                    rotation: 0.7853981633974483,
                    target: "6-seventh-room",
                },
                {
                    yaw: -0.8533222221407613,
                    pitch: 0.2898197106202378,
                    rotation: 5.497787143782138,
                    target: "8-ninth-room",
                },
            ],
            infoHotspots: [
                {
                    yaw: 2.469383021047088,
                    pitch: -0.2645498923854124,
                    title: "Tiny ",
                    text: "Don’t let Tiny’s size fool you! Her heart’s twice as shy, and her wagging tail often stirs up a dust storm. She holds up a shy façade, but quickly makes herself at home when brought into a new environment. Tiny has grown quite fond of her new shelter, making friends along the way. Her art envelops you in a storm of feelings, ultimately dropping you into an expansive calm at the eye of the storm. Why don’t you let her art Rock You Like a Hurricane?",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL8.jpg",
                },
            ],
            opensea:[
                {
                    yaw: -2.469383021047088,
                    pitch: -0.2645498923854124, 
                }
            ]
        },
        {
            id: "8-ninth-room",
            name: "Ninth room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: 3.128774533893174,
                pitch: 0.1426853970313502,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: 0.7727029014521207,
                    pitch: 0.3066958446531629,
                    rotation: 1.5707963267948966,
                    target: "7-eighth-room",
                },
                {
                    yaw: -0.7738449240270313,
                    pitch: 0.27570712570044975,
                    rotation: 5.497787143782138,
                    target: "9-tenth-room",
                },
            ],
            infoHotspots: [
                {
                    yaw: 2.4524140549403155,
                    pitch: -0.26204006318806883,
                    title: "Jury",
                    text: "Jury was found limping in a car parking lot, scavenging and begging for scraps. With our tender love and care, and a lot of treats, Jury found herself filling out an application to join our shelter! After some necessary surgery on her hind legs, Jury now leaps across the many objects scattered around the shelter during her daily playtime. Given her passion for treats, Jury began painting faster than we could put down peanut butter, and you can see a stark streak of eccentricity in each of her paintings.",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL9.jpg",
                },
            ],
            opensea:[
                {
                    yaw: -2.4524140549403155,
                    pitch: -0.26204006318806883,   
                }
            ]
        },
        {
            id: "9-tenth-room",
            name: "Tenth Room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: -2.359025915836643,
                pitch: 0.06828449628589084,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: -0.26149949637053815,
                    pitch: 0.31571610522741445,
                    rotation: 11.780972450961727,
                    target: "8-ninth-room",
                },
                {
                    yaw: 0.33779885222604733,
                    pitch: 0.30886396976420194,
                    rotation: 0.7853981633974483,
                    target: "10-eleventh-room",
                },
            ],
            infoHotspots: [
                {
                    yaw: -3.1227519968459596,
                    pitch: -0.24376535707700953,
                    title: "Bear",
                    text: "Bear has been a staunch compadre to Cooper, and many often think of Bear as Cooper’s right hand dog and advisor. Bear assists Cooper in his grandfatherly duties, at times corralling the younger ones into some structure of discipline. Bear will often be the brave first adventurer whenever it is time for injections, showing the other doggos that it’s not that bad. It’s no surprise then that Bear was one of the first to take to painting. Bear’s artwork inspires everyone at the shelter to be bold, and you’re sure to feel a surging rise to start something new whenever you look at one of his pieces.",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL10.jpg",
                },
            ],
            opensea:[
                {
                    yaw: 3.1227519968459596+1.6,
                    pitch: -0.24376535707700953,  
                }
            ]
        },
        {
            id: "10-eleventh-room",
            name: "Eleventh Room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: -3.084522613155819,
                pitch: 0.18014483104191648,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: -0.9040184872902977,
                    pitch: 0.27993471279940607,
                    rotation: 5.497787143782138,
                    target: "9-tenth-room",
                },
                {
                    yaw: 0.8908499690483644,
                    pitch: 0.28329763304182976,
                    rotation: 0.7853981633974483,
                    target: "0-first-room",
                },
            ],
            infoHotspots: [
                {
                    yaw: 2.3257603471356143,
                    pitch: -0.2396146114727653,
                    title: "Vida",
                    text: "Vida is the warrior of the group, being ready to play not more than a day after her surgery. Vida isn’t scared of sharing her strength with the others, and sees herself willing to take up any challenge that drops on her plate. Maybe that’s why art came so naturally to Vida, because her willpower shines through the strokes, and brighter than a diamond. Catch a glimpse of her art, you’re sure to be left astounded.",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL11.jpg",
                },
            ],
            opensea:[
                {
                    yaw: -2.3257603471356143,
                    pitch: -0.2396146114727653,   
                }
            ]
        },
    ],
    name: "Project Title",
    settings: {
        mouseViewMode: "drag",
        autorotateEnabled: false,
        fullscreenButton: false,
        viewControlButtons: false,
    },
};
